# ideascube case

## Previews

Because the files are to big to be displayed within GitLab,
these links point to an external service which display a preview:

- [`Base.stl`](https://www.viewstl.com/?embedded&url=https%3A%2F%2Fgitlab.com%2Fbibliosansfrontieres%2Fideascube%2Fcase%2F-%2Fraw%2Fmain%2FBase.stl)
- [`Cap.stl`](https://www.viewstl.com/?embedded&url=https%3A%2F%2Fgitlab.com%2Fbibliosansfrontieres%2Fideascube%2Fcase%2F-%2Fraw%2Fmain%2FCap.stl)
- [`IO-Plate.stl`](https://www.viewstl.com/?embedded&url=https%3A%2F%2Fgitlab.com%2Fbibliosansfrontieres%2Fideascube%2Fcase%2F-%2Fraw%2Fmain%2FIO-Plate.stl)
